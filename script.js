"use strict"

let thisInterval;
let timerTime;
let showImages = ["./img/1.jpg", "./img/2.jpg", "./img/3.jpg", "./img/4.png"],
 a = -1;

function time () {
    let time_go = 3;
    downloadTimer = setInterval(function () {
        document.getElementById("timer").innerText = time_go;
        time_go -= 1;
        if (time_go <= 0) 
            clearInterval(downloadTimer);
    }, 3000);
}

function displayShowImages() {
    a = (a === showImages.length - 1) ? 0 : a + 1;
    document.getElementById("img").src = showImages[a];
}

thisInterval = setInterval(displayShowImages, 3000);
timerTime = setInterval(time, 3000);

document.getElementById("stop").addEventListener('click', function () {
    clearInterval(thisInterval);
    clearInterval(timerTime);
    document.getElementById("stop").setAttribute('disabled', 'disabled');
    document.getElementById("resume").removeAttribute("disabled");

});

document.getElementById("resume").addEventListener('click', function () {
    thisInterval = setInterval(displayShowImages, 3000);
    timerTime = setInterval(time, 3000);
    document.getElementById("resume").setAttribute('disabled', 'disabled');
    document.getElementById("stop").removeAttribute("disabled");

});





